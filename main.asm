%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUFFER_SIZE 256

section .rodata
string_error: db "string size exceeds buffer size", 0
not_found_404: db "key not found", 0
section .data
new_word: db 0

global _start

section .text

_start:
	mov rdi, new_word
	mov rsi, BUFFER_SIZE
	call read_string
	test rax, rax
	jz .string_error
	mov rdi, rax
	mov rsi, FUTURE_KEY
	push rdi
	call find_word
	pop rdi
	test rax, rax
	jz .not_found
	mov r12, rax
	mov rdi, rax
	call string_length
	add r12, rax
	inc r12
	mov rdi, r12
	call print_string
	xor rdi, rdi
	call exit

	.string_error:
	mov rdi, string_error
	jmp .error

	.not_found:
	mov rdi, not_found_404

	.error:
	call print_string
	call exit
