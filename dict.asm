%include 'lib.inc'
%define BYTE 8

section .text

global find_word

find_word:
	test rsi, rsi
  	jz .exit
	.loop:
	push rdi
	add rsi, BYTE
	push rsi
	call string_equals
	pop rsi
	pop rdi
	test rax, rax
	jnz .end
	sub rsi, BYTE
	mov rsi, qword[rsi]
	test rsi, rsi
	jnz .loop
        jmp .exit

	.end:
	mov rax, rsi
	ret

	.exit:
	xor rax, rax
	ret
