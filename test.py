import subprocess

input_list = ["fourth word", "second word", '',  "w" * 256]
output_list = ['key not found', "second word explanation", "key not found", "string size exceeds buffer size"]

for i in range(len(input_list)):
	subprcs = subprocess.Popen(["./main"], stdin = subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
	stdout, stderr = subprcs.communicate(input=input_list[i].encode())
	list_output =  stdout.decode().strip()
	if list_output == output_list[i]:
		print("Passed test {}".format(i + 1))
	else:
		print ("Invalid test {}".format(i + 1 ))
		print("Expected: ", output_list[i])
		print("Recieved: ", list_output)   
