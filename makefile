ASM = nasm
ASMFLAGS = -felf64
PYTHON = python3

.PHONY: test clean run

.all: clean lib.o, main.o, dict.o, main

main: main.o dict.o lib.o
	ld -o $@ $^

main.o: main.asm dict.o lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $^

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<

test:main 
	$(PYTHON) test.py
	
clean:
	rm -rf *.o
	rm main

run:
	main

